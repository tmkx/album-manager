import PhotosUI
import Photos
import SwiftUI

struct ContentView: View {
    @State private var startDate = Date()
    @State private var endDate = Date()
    @State private var showingNoAssetsAlert = false
    @State private var showingDoneAlert = false
    @State private var doneAlertContent = ""

    var body: some View {
        VStack {
            DatePicker("Start Date", selection: $startDate, displayedComponents: [.date])
            DatePicker("End Date", selection: $endDate, displayedComponents: [.date])

            Button("Cleanup") {
                let status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
                if (status == PHAuthorizationStatus.authorized) {
                    print("Authorized")
                    let fetchOptions = PHFetchOptions()
                    fetchOptions.predicate = NSPredicate(
                        format: "(creationDate >= %@) AND (creationDate <= %@)",
                        startDate as NSDate,
                        endDate as NSDate
                    )
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
                    let assets = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                    print("assets count: \(assets.count)")
                    if assets.count == 0 {
                        showingNoAssetsAlert = true
                        return
                    }
                    var notFavoriteAssets: [PHAsset] = []
                    for i in 0..<assets.count {
                        let asset = assets.object(at: i)
                        if (!asset.isFavorite) { notFavoriteAssets.append(asset) }
                    }
                    print("notFavoriteAssets count: \(notFavoriteAssets.count)")
                    if (!notFavoriteAssets.isEmpty) {
                        let collectionTitle = "To be deleted"
                        let collections = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: nil)
                        var foundExistAlbum = false
                        var albumIdentifier = ""
                        for i in 0..<collections.count {
                            if (collections.object(at: i).localizedTitle == collectionTitle) {
                                foundExistAlbum = true
                                albumIdentifier = collections.object(at: i).localIdentifier
                                break
                            }
                        }
                        print("foundExistAlbum: \(foundExistAlbum)")
                        PHPhotoLibrary.shared().performChanges({
                            if (!foundExistAlbum) {
                                albumIdentifier = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: collectionTitle).placeholderForCreatedAssetCollection.localIdentifier
                            }
                            print("albumIdentifier: \(albumIdentifier)")
                        }) { (success, error) in
                            if (!success) {
                                doneAlertContent = "Create Album Failed: \(error?.localizedDescription ?? "Unknown")"
                                showingDoneAlert = true
                                return
                            }
                            PHPhotoLibrary.shared().performChanges({
                                // PHAssetChangeRequest.deleteAssets(notFavoriteAssets as NSArray)
                                let albumCollection = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [albumIdentifier], options: nil).firstObject!
                                let request = PHAssetCollectionChangeRequest(for: albumCollection)
                                request?.addAssets(notFavoriteAssets as NSFastEnumeration)
                            }) { (success, error) in
                                if (success) {
                                    doneAlertContent = "Moved \(notFavoriteAssets.count) asset(s) to the album"
                                } else {
                                    doneAlertContent = "Failed: \(error?.localizedDescription ?? "Unknown")"
                                }
                                showingDoneAlert = true
                            }
                        }
                    }
                } else {
                    print("Not Authorized")
                    PHPhotoLibrary.requestAuthorization(for: .readWrite) { newStatus in
                        if (newStatus == PHAuthorizationStatus.authorized) {
                            print("Authed")
                        } else {
                            print("Status: \(newStatus)")
                        }
                    }
                }
            }
            .alert("No assets found", isPresented: $showingNoAssetsAlert) {
                Button("OK", role: .cancel) {}
            }
            .alert(doneAlertContent, isPresented: $showingDoneAlert) {
                Button("OK", role: .cancel) {}
            }
       }
       .padding()
    }
}

#Preview {
    ContentView()
}
