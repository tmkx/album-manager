import SwiftUI

@main
struct AlbumManagerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
